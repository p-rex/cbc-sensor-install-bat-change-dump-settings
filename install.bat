REM ===== Variables ===== 
REM CB Install Options
set INSTALLER_PATH=
set COMPANY_CODE=
set GROUP_NAME=
set CLI_USERS=
set INSTALL_LOG=%TEMP%\cb_install_log.txt

REM Other Variables
set CB_DUMP_SETTINGS=ConfigureMemoryDumpSettings=0
set CFG_INI="C:\Program Files\Confer\cfg.ini"
set REPCLI="C:\Program Files\Confer\repcli.exe"


REM ===== Install ===== 
msiexec.exe /q /i "%INSTALLER_PATH%" /L* "%INSTALL_LOG%" COMPANY_CODE="%COMPANY_CODE%" GROUP_NAME=%GROUP_NAME% CLI_USERS=%CLI_USERS%

	

REM ===== Wait till installation finish ===== 
ping -n 10 localhost > nul

:check_install
	%REPCLI% status | findstr Registered | findstr Yes
	if "%ERRORLEVEL%"=="0" (
		goto :installation_finish
	) else (
		ping -n 3 localhost > nul
	)	

goto :check_install

:installation_finish


REM  ===== Change Dump Settings ===== 

REM Wait, just in case
ping -n 5 localhost > nul

REM Enable Bypass
%REPCLI% bypass 1

REM Backup cfg.ini
copy %CFG_INI% %TEMP%

REM Add CB DumpSettings
findstr %CB_DUMP_SETTINGS% %CFG_INI%
if "%ERRORLEVEL%"=="1" (
	(echo %CB_DUMP_SETTINGS%) >> %CFG_INI%
)


REM Change Win DumpSettings to Auto
reg add "HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\CrashControl" /v "CrashDumpEnabled" /t "REG_DWORD" /d "7" /f

REM Update CB Sensor config
%REPCLI% updateConfig

REM Disable Bypass
%REPCLI% bypass 0

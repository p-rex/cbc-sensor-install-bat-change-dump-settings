# CBC Sensor install bat - default dump settings

This is a bat file to install CBC Sensor with default dump settings (automatic memory dump)

## Usage

1) Edit CBC Install options.

```
set INSTALLER_PATH=C:\test\installer.msi
set COMPANY_CODE=XXXXXXXXXXXXX
set GROUP_NAME=Standard-Policy
set CLI_USERS=S-1-1-0
```

*Notes: If specified string has space, you must enclose it in double quote ("").*


2) Save the file as .bat file.

3) Execute the bat as Administrator. 

## Tested version
Windows 10 Enterprise Ja  
CBC Sensor v3.5.0.1607
